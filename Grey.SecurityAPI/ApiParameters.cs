﻿using System.Collections.Generic;

namespace Grey.SecurityAPI
{
    public class ApiParameters : IApiParameters
    {
        public string SecurityApiLoginEndpoint { get; set; }
        public string SecurityApiOAuthEndpoint { get; set; }
        public string SecurityApiClientId { get; set; }
        public string SecurityApiClientSecret { get; set; }

        public string AzureTenantId { get; set; }

        public string CosmosDBEndpoint { get; set; }
        public string CosmosDBEndpointKey { get; set; }
        public string CosmosDBDatabaseName { get; set; }
        public string CosmosDBCollectionName { get; set; }
        public string CosmosDBTokenTTL { get; set; }

        public Dictionary<string, string> LoginRedirectPages { get; set; }

        public string SearchPlatformUrl { get; set; }

        public string AzureSearchApiUrl { get; set; }
        public string AzureSearchApiVersion { get; set; }
        public string AzureSearchApiKey { get; set; }
        public string AzureFunctionSearchUrl { get; set; }

        public string SearchUrl { get; set; }
        public string SearchApiKey { get; set; }

        public string GroupsConfig { get; set; }
        public string MaxCnt { get; set; }
        public string IsDirectCalls { get; set; }

        public string SpellcheckUrl { get; set; }
        public string SpellcheckKey { get; set; }
        public string SpellcheckMode { get; set; }
        public string SpellcheckMkt { get; set; }
    }
}