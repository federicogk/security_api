﻿using Grey.SecurityAPI.Helpers;
using Grey.SecurityAPI.Model;
using Grey.SecurityAPI.Storage;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Grey.SecurityAPI.Controllers
{
    /// <summary>
    /// Controller to replace the Shibboleth based authentication system.
    /// </summary>
    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        const string authorizeEndpointPattern = "https://login.microsoftonline.com/{0}/oauth2/authorize?client_id={1}&response_type=id_token&response_mode=form_post&redirect_uri={2}&scope={3}&state={4}&nonce={4}";
        
        readonly Guid _nonce;
        ITokenStorage _storage;
        IApiParameters _parameters;
        IUrlHelperMethods _urlHelper;

        public LoginController(ITokenStorage storage, IApiParameters parameters, IUrlHelperMethods urlHelper)
        {
            _nonce = Guid.NewGuid();
            _storage = storage;
            _urlHelper = urlHelper;
            _parameters = parameters;
        }
        

        [HttpGet]
        public ActionResult Get()
        {
            string caller = _urlHelper.GetCallerUrl(Request.Headers);

            return Redirect(String.Format(authorizeEndpointPattern, _parameters.AzureTenantId, _parameters.SecurityApiClientId, _parameters.SecurityApiLoginEndpoint, "openid", caller, _nonce));
        }

        [HttpPost]
        public async Task<ActionResult> Post()
        {
            Guid token = Guid.Empty;

            var idToken = Request.Form["id_token"];

            string callerUrl = Request.Form["state"];

            try
            {
                var jwtIdToken = new JwtSecurityToken(idToken);

                UserInfo existingSession = await _storage.GetCurrentSession(jwtIdToken);

                if (existingSession == null)
                {
                    token = await _storage.Create(jwtIdToken, idToken);
                }
                else
                {
                    token = existingSession.Id;
                }
            }
            catch (Exception)
            {

                throw;
            }
            string clientRedirectionPage = _urlHelper.CreateTokenResponseUrl(callerUrl, token.ToString());
            return Redirect(clientRedirectionPage);
        }
    }
}

// To support FE integration with Shibboleth we need Security API do following:

//1.	POST call from Shibboleth with SAMLResponse handling (see details in prototype)
//	* Read SAML2Token from SAMLResponse
//	* Read claims from token
//	* Save claims to CosmosDB with configurable ttl 
//	* Return Document.Id to FE as an lightweight token (JWT like)

//2.	Method to GET claims and ttl on a basis of token.
//3.	DELETE method to delete token from CosmosDB during user logout

//Architecture –https://grydev.atlassian.net/wiki/spaces/GRE004/pages/255098967/Architecture

//Process - https://grydev.atlassian.net/wiki/spaces/GRE004/pages/251592868/SSO+integration

//Prototype:

//    DocumentClient client = new DocumentClient(new Uri(EndpointUrl), PrimaryKey);
//    var claims = new Dictionary<string, object>();
//    var requestContent = req.Content;
//    var data = await requestContent.ReadAsStringAsync();
//    var formParams = HttpUtility.ParseQueryString(data ?? "");
//    var responseKey = System.Configuration.ConfigurationManager.AppSettings["RESPONSE_KEY"];
//    var samlToken = (Saml2SecurityToken)null;
//    var decodedSamlResponse = Convert.FromBase64String(formParams[responseKey]);
    
//    var reader = XmlReader.Create(new MemoryStream(decodedSamlResponse));
//    var serializer = new XmlSerializer(typeof(XmlElement));
//    var samlResponseElement = (XmlElement)serializer.Deserialize(reader);
//    var manager = new XmlNamespaceManager(samlResponseElement.OwnerDocument.NameTable);
//    manager.AddNamespace("saml2", "urn:oasis:names:tc:SAML:2.0:assertion");
//    var assertion = (XmlElement)samlResponseElement.SelectSingleNode("//saml2:Assertion", manager);
//    SecurityTokenHandlerCollection collection = SecurityTokenHandlerCollection.CreateDefaultSecurityTokenHandlerCollection();
//    samlToken = (Saml2SecurityToken)collection.ReadToken(XmlReader.Create(new StringReader(assertion.OuterXml)));

//    claims.Add("ttl", ttl);

//    foreach (var statement in samlToken.Assertion.Statements)
//    {
//        Saml2AttributeStatement attributeStatement = statement as Saml2AttributeStatement;                
//        if (attributeStatement != null)
//        {
//            foreach (var attribute in attributeStatement.Attributes)
//            {
//                claims.Add(attribute.FriendlyName, attribute.Values);
//            }
//        }
//    }

//    Uri collectionUri = UriFactory.CreateDocumentCollectionUri(DatabaseName, CollectionName);
//    var document = await client.CreateDocumentAsync(collectionUri, claims);

//    var response = req.CreateResponse(HttpStatusCode.Moved);
//    var url = System.Configuration.ConfigurationManager.AppSettings["SEARCH_PLATFORM_URL"];
//    response.Headers.Location = new Uri(url + document.Resource.Id);
//    return response;

//POC URL - https://gpfrontend.azureedge.net/gpfrontend/spike/2018-02-15T10:48:53.558Z/index.html#/search
// * /
