﻿using Grey.SecurityAPI.Helpers;
using Grey.SecurityAPI.Model;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Grey.SecurityAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/OAuth")]
    public class OAuthController : Controller
    {
        const string oauthAccessCodeRequest = "https://login.microsoftonline.com/{0}/oauth2/authorize?client_id={1}&response_type=code&response_mode=form_post&redirect_uri={2}&state={3}";
        const string oauthTokenRequest = "https://login.microsoftonline.com/{0}/oauth2/token";

        IUrlHelperMethods _urlHelper;
        IApiParameters _parameters;

        public OAuthController(IApiParameters parameters, IUrlHelperMethods urlHelper)
        {
            _urlHelper = urlHelper;
            _parameters = parameters;
        }

        [HttpGet]
        public ActionResult Get()
        {
            string caller = _urlHelper.GetCallerUrl(Request.Headers);

            return Redirect(String.Format(oauthAccessCodeRequest, _parameters.AzureTenantId, _parameters.SecurityApiClientId, _parameters.SecurityApiOAuthEndpoint, caller));
        }

        [HttpPost]
        public ActionResult Post()
        {
            var code = Request.Form["code"];

            string callerUrl = Request.Form["state"];

            string clientRedirectionPage = _urlHelper.CreateTokenResponseUrl(callerUrl, code);

            return Redirect(clientRedirectionPage);
        }

        [HttpPost("Token")]
        public async Task<JsonResult> Token([FromBody] TokenRequest tokenRequest)
        {
            HttpResponseMessage response = null;
            OAuthInfo oAuthInfo = new OAuthInfo();
            try
            {
                string url = String.Format(oauthTokenRequest, _parameters.AzureTenantId);

                string contentString = String.Format("grant_type={0}&client_id={1}&code={2}&redirect_uri={3}&resource={4}&client_secret={5}",
                                                    WebUtility.UrlEncode("authorization_code"),
                                                    WebUtility.UrlEncode(_parameters.SecurityApiClientId),
                                                    WebUtility.UrlEncode(tokenRequest.code),
                                                    WebUtility.UrlEncode(_parameters.SecurityApiOAuthEndpoint),
                                                    WebUtility.UrlEncode(_parameters.SecurityApiClientId),
                                                    WebUtility.UrlEncode(_parameters.SecurityApiClientSecret));

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Accept", "application/json");
                    var content = new StringContent(contentString, System.Text.Encoding.UTF8, "application/x-www-form-urlencoded");

                    response = await client.PostAsync(url, content);

                    if (response.IsSuccessStatusCode)
                    {
                        string reponseContent = await response.Content.ReadAsStringAsync();

                        var jsonResponse = JObject.Parse(reponseContent);

                        oAuthInfo.Token.AccessToken = jsonResponse["access_token"].ToString();
                        oAuthInfo.Token.RefreshToken = jsonResponse["refresh_token"].ToString();
                        oAuthInfo.Token.IdToken = jsonResponse["id_token"].ToString();
                        oAuthInfo.Token.ExpiresOn = jsonResponse["expires_on"].ToString();

                        var jwt = new JwtSecurityToken(oAuthInfo.Token.IdToken);
                        oAuthInfo.Profile.UserId = TryGetClaim(jwt, "oid");
                        oAuthInfo.Profile.Email = TryGetClaim(jwt, "upn");
                    }
                }
                
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }

            return Json(oAuthInfo);
        }

        private string TryGetClaim(JwtSecurityToken token, string key)
        {
            string value = string.Empty;
            if (token.Claims.Any(x => x.Type == key))
            {
                var claim = token.Claims.First(x => x.Type == key);
                value = claim.Value;
            }
            return value;
        }
    }

    public class TokenRequest
    {
        public string code { get; set; }

        public string redirectUrl { get; set; }
    }
}