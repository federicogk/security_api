﻿using Grey.SecurityAPI.Model;
using Grey.SecurityAPI.Storage;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Grey.SecurityAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        const string logoutEndpointPattern = "https://login.microsoftonline.com/957cceeb-9829-4dd9-a680-8ca4d2fe58f0/oauth2/logout?id_token_hint={0}&post_logout_redirect_uri{1}";
        ITokenStorage _storage;
        public UserController(ITokenStorage storage)
        {
            _storage = storage;
        }

        // GET: api/User
        [HttpGet("{token}")]
        public async Task<JsonResult> Get(string token)
        {
            UserInfo user = null;
            try
            {
                user = await _storage.GetCurrentSession(token);
            }
            catch (Exception)
            {

            }
            return Json(BasicUserInfo.From(user));
        }

        [HttpGet("IsAuthenticated/{token}")]
        public async Task<bool> IsAuthenticated(string token)
        {
            bool isAuthenticated = false;
            try
            {
                isAuthenticated = await _storage.Exists(token);
            }
            catch (Exception)
            {

                throw;
            }
            return isAuthenticated;
        }

        // DELETE api/values/5
        [HttpDelete("{token}")]
        public async Task<bool> Delete(string token)
        {
            bool result = false;

            try
            {
                UserInfo user = await _storage.GetCurrentSession(token);
                await _storage.Delete(token);

                string logoutUrl = String.Format(logoutEndpointPattern, user.IdToken, "google.com");
                using (var http = new HttpClient())
                {
                    var loginRequest = await http.GetAsync(logoutUrl);

                    result = loginRequest.IsSuccessStatusCode;
                }
            }
            catch (Exception)
            {

                throw;
            }
            
            return result;
        }
    }
}
