﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SecurityAPI.Helpers
{
    public interface IUrlHelperMethods
    {
        string GetCallerUrl(IHeaderDictionary headers);

        string TryGetRedirectionPath(string url);

        string CreateTokenResponseUrl(string caller, string token);
    }
}
