﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace Grey.SecurityAPI.Helpers
{
    public class UrlHelperMethods : IUrlHelperMethods
    {
        IApiParameters _parameters;
        public UrlHelperMethods(IApiParameters apiParameters)
        {
            _parameters = apiParameters;
        }

        public string CreateTokenResponseUrl(string caller, string token)
        {
            Uri callerUrl = new Uri(caller);

            string redirectionPath = TryGetRedirectionPath(callerUrl.Authority);

            return String.Format("{0}{1}{2}", callerUrl, redirectionPath, token);
        }

        public string GetCallerUrl(IHeaderDictionary headers)
        {
            string caller = "";
            StringValues referer = new StringValues();

            if (headers.TryGetValue("Referer", out referer))
            {
                Uri uri = new Uri(referer);

                caller = uri.GetLeftPart(UriPartial.Authority);
            }
            return caller;
        }

        public string TryGetRedirectionPath(string url)
        {
            string redirectPage = string.Empty;
            if (!_parameters.LoginRedirectPages.TryGetValue(url.Replace(":", "_"), out redirectPage))
            {
                redirectPage = url;
            }
            return redirectPage;
        }
    }
}
