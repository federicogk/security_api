﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SecurityAPI
{
    public interface IApiParameters
    {
        string SecurityApiLoginEndpoint { get; set; }
        string SecurityApiOAuthEndpoint { get; set; }
        string SecurityApiClientId { get; set; }
        string SecurityApiClientSecret { get; set; }
        string AzureTenantId { get; set; }
        
        string CosmosDBEndpoint { get; set; }
        string CosmosDBEndpointKey { get; set; }
        string CosmosDBDatabaseName { get; set; }
        string CosmosDBCollectionName { get; set; }
        string CosmosDBTokenTTL { get; set; }

        Dictionary<string, string> LoginRedirectPages { get; set; }

        string SearchPlatformUrl { get; set; }

        string AzureSearchApiUrl { get; set; }
        string AzureSearchApiVersion { get; set; }
        string AzureSearchApiKey { get; set; }
        string AzureFunctionSearchUrl { get; set; }

        string SearchUrl { get; set; }
        string SearchApiKey { get; set; }

        string GroupsConfig { get; set; }
        string MaxCnt { get; set; }
        string IsDirectCalls { get; set; }

        string SpellcheckUrl { get; set; }
        string SpellcheckKey { get; set; }
        string SpellcheckMode { get; set; }
        string SpellcheckMkt { get; set; }
    }
}