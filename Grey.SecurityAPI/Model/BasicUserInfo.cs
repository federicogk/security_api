﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SecurityAPI.Model
{
    public class BasicUserInfo
    {
        public static BasicUserInfo From(UserInfo userInfo)
        {
            BasicUserInfo info = null;
            if (userInfo != null)
            {
                info = new BasicUserInfo
                {
                    Id = userInfo.Id.ToString(),
                    Sn = userInfo.FamilyName,
                    GivenName = userInfo.GivenName,
                    Title = userInfo.Name,
                    Email = userInfo.Email
                };
            }
            return info;
        }


        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("sn")]
        public string Sn { get; set; }

        [JsonProperty("givenName")]
        public string GivenName { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }
}