﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SecurityAPI.Model
{
    public class OAuthInfo
    {
        public OAuthInfo()
        {
            Profile = new OAuthUserProfile();
            Token = new OAuthToken();
        }

        [JsonProperty("token")]
        public OAuthToken Token { get; set; }

        [JsonProperty("profile")]
        public OAuthUserProfile Profile { get; set; }
    }

    public class OAuthUserProfile
    {
        [JsonProperty("mail")]
        public string Email { get; set; }

        [JsonProperty("id")]
        public string UserId { get; set; }
    }

    public class OAuthToken
    {
        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }

        [JsonProperty("idToken")]
        public string IdToken { get; set; }

        [JsonProperty("expiresOn")]
        public string ExpiresOn { get; set; }
    }

}
