﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SecurityAPI.Model
{
    public class UserInfo
    {
        public UserInfo()
        {
            if (Id == Guid.Empty)
            {
                Id = Guid.NewGuid();
                CreatedDate = DateTime.UtcNow;
            }
        }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("idToken")]
        public string IdToken { get; set; }

        [JsonProperty("upn")]
        public string Upn { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("familyName")]
        public string FamilyName { get; set; }

        [JsonProperty("givenName")]
        public string GivenName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        // used to set expiration policy
        [JsonProperty(PropertyName = "ttl", NullValueHandling = NullValueHandling.Ignore)]
        public int? TimeToLive { get; set; }
    }
}
