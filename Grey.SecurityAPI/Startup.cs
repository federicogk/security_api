﻿using Grey.SecurityAPI.Helpers;
using Grey.SecurityAPI.Storage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Grey.SecurityAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            IApiParameters parameters = Configuration.GetSection("apiParameters").Get<ApiParameters>();
            services.AddCors();
            services.AddMvc();
            services.AddSingleton<IApiParameters>(parameters);
            services.AddTransient<IUrlHelperMethods, UrlHelperMethods>();
            services.AddSingleton<IDocumentClient>(new DocumentClient(new Uri(parameters.CosmosDBEndpoint), parameters.CosmosDBEndpointKey));
            services.AddTransient<ITokenStorage, CosmosDBStorage>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().Build());
            app.UseMvc();
        }
    }
}
