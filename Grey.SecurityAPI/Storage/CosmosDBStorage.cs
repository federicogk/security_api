﻿using Grey.SecurityAPI.Model;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SecurityAPI.Storage
{
    public class CosmosDBStorage : ITokenStorage
    {
        private IDocumentClient _client;
        private IApiParameters _parameters;
        public CosmosDBStorage(IDocumentClient client, IApiParameters parameters)
        {
            _client = client;
            _parameters = parameters;
        }

        public async Task<Guid> Create(JwtSecurityToken jwtToken, string originalToken)
        {
            Guid id = Guid.Empty;
            try
            {
                UserInfo user = new UserInfo();
                user.IdToken = originalToken;
                user.Upn = TryGetClaim(jwtToken, "upn");
                user.GivenName = TryGetClaim(jwtToken, "given_name"); 
                user.FamilyName = TryGetClaim(jwtToken, "family_name"); 
                user.Email = TryGetClaim(jwtToken, "unique_name");
                user.Name = TryGetClaim(jwtToken, "name");

                int ttl = 0;
                if (Int32.TryParse(_parameters.CosmosDBTokenTTL, out ttl))
                {
                    user.TimeToLive = ttl;
                }

                Document doc = await _client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(_parameters.CosmosDBDatabaseName, _parameters.CosmosDBCollectionName) , user);

                id = user.Id;
            }
            catch (Exception ex)
            {
                throw;
            }

            return id;
        }

        public async Task<bool> Exists(string token)
        {
            bool exists = false;
            try
            {
                if (!String.IsNullOrEmpty(token))
                {
                    Guid guidToken = new Guid(token);
                    exists = await _client.CreateDocumentQuery<UserInfo>(UriFactory.CreateDocumentCollectionUri(_parameters.CosmosDBDatabaseName, _parameters.CosmosDBCollectionName))
                                                                                    .Where(x => x.Id == guidToken)
                                                                                    .ToAsyncEnumerable()
                                                                                    .Any();
                }
            }
            catch (Exception ex)
            {

            }
            
            return exists;
        }

        public async Task<UserInfo> GetCurrentSession(string token)
        {
            UserInfo existingSession = null;
            try
            {
                if (!String.IsNullOrEmpty(token))
                {
                    Guid guidToken = new Guid(token);
                    existingSession = await _client.CreateDocumentQuery<UserInfo>(UriFactory.CreateDocumentCollectionUri(_parameters.CosmosDBDatabaseName, _parameters.CosmosDBCollectionName))
                                                    .Where(so => so.Id == guidToken)
                                                    .ToAsyncEnumerable()
                                                    .FirstOrDefault();
                }

            }
            catch (Exception ex)
            {

            }

            return existingSession;
        }

        public async Task<UserInfo> GetCurrentSession(JwtSecurityToken jwtToken)
        {
            UserInfo existingSession = null;
            try
            {
                string upn = TryGetClaim(jwtToken, "upn");

                if (!String.IsNullOrEmpty(upn))
                {
                    existingSession = await _client.CreateDocumentQuery<UserInfo>(UriFactory.CreateDocumentCollectionUri(_parameters.CosmosDBDatabaseName, _parameters.CosmosDBCollectionName))
                                                    .Where(so => so.Upn == upn)
                                                    .ToAsyncEnumerable()
                                                    .FirstOrDefault();
                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return existingSession;
        }

        public async Task<bool> Delete(string token)
        {
            var doc = await _client.DeleteDocumentAsync(UriFactory.CreateDocumentUri(_parameters.CosmosDBDatabaseName, _parameters.CosmosDBCollectionName, token));

            return !await Exists(token);
        }

        private string TryGetClaim(JwtSecurityToken token, string key)
        {
            string value = string.Empty;
            if (token.Claims.Any(x => x.Type == key))
            {
                var claim = token.Claims.First(x => x.Type == key);
                value = claim.Value;
            }
            return value;
        }
    }
}
