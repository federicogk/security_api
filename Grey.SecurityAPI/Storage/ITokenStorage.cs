﻿using Grey.SecurityAPI.Model;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;

namespace Grey.SecurityAPI.Storage
{
    public interface ITokenStorage
    {
        Task<Guid> Create(JwtSecurityToken jwtToken, string originalToken);

        Task<bool> Exists(string token);

        Task<UserInfo> GetCurrentSession(JwtSecurityToken jwtToken);

        Task<UserInfo> GetCurrentSession(string token);

        Task<bool> Delete(string token);
    }
}
