﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Grey.SecurityAPI.Model;
using Microsoft.EntityFrameworkCore;

namespace Grey.SecurityAPI.Storage
{
    public class SQLStorage : ITokenStorage
    {
        public async Task<Guid> Create(JwtSecurityToken jwtToken, string originalToken)
        {
            UserInfo user = new UserInfo();
            user.IdToken = originalToken;
            user.Upn = jwtToken.Claims.FirstOrDefault(x => x.Type == "upn").Value;
            user.GivenName = jwtToken.Claims.FirstOrDefault(x => x.Type == "given_name").Value;
            user.FamilyName = jwtToken.Claims.FirstOrDefault(x => x.Type == "family_name").Value;
            user.Email = jwtToken.Claims.FirstOrDefault(x => x.Type == "unique_name").Value;
            user.Name = jwtToken.Claims.FirstOrDefault(x => x.Type == "name").Value;

            using (var db = new SecurityContext())
            {
                await db.Users.AddAsync(user);
                await db.SaveChangesAsync();
            }

            return user.Id;
        }

        public async Task<bool> Delete(string token)
        {
            bool result = false;
            using (var db = new SecurityContext())
            {
                var user = await db.Users.FirstOrDefaultAsync(x => x.Id.Equals(token));
                if (user != null)
                {
                    db.Users.Remove(user);
                    result = db.SaveChanges() != 0;
                }
            }
            return result;
        }

        public async Task<bool> Exists(string token)
        {
            bool exists = false;

            using (var db = new SecurityContext())
            {
                exists = await db.Users.AnyAsync(x => x.Id.Equals(token));
            }
            return exists;
        }

        public async Task<UserInfo> GetCurrentSession(JwtSecurityToken jwtToken)
        {
            UserInfo existingSession = null;
            string upn = jwtToken.Claims.FirstOrDefault(x => x.Type == "upn").Value;

            if (!String.IsNullOrEmpty(upn))
            {
                using (var db = new SecurityContext())
                {
                    existingSession = await db.Users.FirstOrDefaultAsync(x => x.Upn.Equals(upn));
                }
            }

            return existingSession;
        }

        public async Task<UserInfo> GetCurrentSession(string token)
        {
            UserInfo existingSession = null;
            
            if (!String.IsNullOrEmpty(token))
            {
                Guid guidToken = new Guid(token);
                using (var db = new SecurityContext())
                {
                    existingSession = await db.Users.FirstOrDefaultAsync(x => x.Id.Equals(guidToken));
                }
            }

            return existingSession;
        }
    }
}
