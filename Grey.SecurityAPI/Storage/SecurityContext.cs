﻿using Grey.SecurityAPI.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grey.SecurityAPI.Storage
{
    public class SecurityContext : DbContext
    {
        const string conn = @"Data Source=AR-IT01892\SQLEXPRESS;Initial Catalog=SecurityDatabase; Persist Security Info=True; Integrated Security=true; Connection Timeout=3600; MultipleActiveResultSets=True;";
        public SecurityContext()
            : base()
        {
            
        }

        public DbSet<UserInfo> Users { get { return Set<UserInfo>(); } }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(conn, null);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<UserInfo>().ToTable("UserInfo");
            modelBuilder.Entity<UserInfo>().HasKey(x => x.Id);
        }
    }
}
